import os
import json
import torch
import shutil
import logging

from PIL import Image
from skimage.color import lab2rgb
from torch.autograd import Variable


class Params:
    def __init__(self, json_path):
        with open(json_path) as f:
            params = json.load(f)
            self.__dict__.update(params)

    def save(self, json_path):
        with open(json_path, 'w') as f:
            json.dump(self.__dict__, f, indent=4)

    def update(self, json_path):
        """Loads parameters from json file"""
        with open(json_path) as f:
            params = json.load(f)
            self.__dict__.update(params)

    @property
    def dict(self):
        """Gives dict-like access to Params instance by `params.dict['learning_rate']"""
        return self.__dict__


class RunningAverage:
    """A simple class that maintains the running average of a quantity
    """
    def __init__(self):
        self.steps = 0
        self.total = 0

    def update(self, val):
        self.total += val
        self.steps += 1

    def __call__(self):
        return self.total/float(self.steps)


def set_logger(log_path):
    """Set the logger to log info in terminal and file `log_path`.

    In general, it is useful to have a logger so that every output to the terminal is saved
    in a permanent file. Here we save it to `model_dir/train.log`.
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    if not logger.handlers:
        # Logging to a file
        file_handler = logging.FileHandler(log_path)
        file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
        logger.addHandler(file_handler)

        # Logging to console
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(stream_handler)


def save_dict_to_json(d, json_path):
    """Saves dict of floats in json file

    Args:
        d: (dict) of float-castable values (np.float, int, float, etc.)
        json_path: (string) path to json file
    """
    with open(json_path, 'w') as f:
        # We need to convert the values to float for json (it doesn't accept np.array, np.float, )
        d = {k: float(v) for k, v in d.items()}
        json.dump(d, f, indent=4)


def save_checkpoint(state, is_best, checkpoint):
    """Saves model and training parameters at checkpoint + 'last.pth.tar'. If is_best==True, also saves
    checkpoint + 'best.pth.tar'

    Args:
        state: (dict) contains model's state_dict, may contain other keys such as epoch, optimizer state_dict
        is_best: (bool) True if it is the best model seen till now
        checkpoint: (string) folder where parameters are to be saved
    """
    filepath = os.path.join(checkpoint, 'last.pth.tar')
    if not os.path.exists(checkpoint):
        print("Checkpoint Directory does not exist! Making directory {}".format(checkpoint))
        os.mkdir(checkpoint)
    else:
        print("Checkpoint Directory exists! ")
    torch.save(state, filepath)
    if is_best:
        shutil.copyfile(filepath, os.path.join(checkpoint, 'best.pth.tar'))


def load_checkpoint(checkpoint, model, optimizer=None):
    """Loads model parameters (state_dict) from file_path. If optimizer is provided, loads state_dict of
    optimizer assuming it is present in checkpoint.

    Args:
        checkpoint: (string) filename which needs to be loaded
        model: (torch.nn.Module) model for which the parameters are loaded
        optimizer: (torch.optim) optional: resume optimizer from checkpoint
    """
    if not os.path.exists(checkpoint):
        raise("File doesn't exist {}".format(checkpoint))
    checkpoint = torch.load(checkpoint)
    model.load_state_dict(checkpoint['state_dict'])

    if optimizer:
        optimizer.load_state_dict(checkpoint['optim_dict'])

    return checkpoint


def print_result(output_ab, input_ab, input_l):
    output_ab = Variable(output_ab)
    out_ab_cpu = output_ab.cpu()
    in_ab_cpu = input_ab.cpu()
    in_l_cpu = input_l.cpu()

    in_ab_base = in_ab_cpu[0]
    out_ab_base = out_ab_cpu[0]
    in_l_base = in_l_cpu[0]

    in_l_img_arr = in_l_base.numpy().reshape(224, 224) * 255
    in_l_image = Image.fromarray(in_l_img_arr)

    # Create input and output LAB
    in_lab_base = torch.cat((in_l_base, in_ab_base), 0).numpy()
    out_lab_base = torch.cat((in_l_base, out_ab_base), 0).numpy()

    # Transpose base images
    in_lab = in_lab_base.transpose((1, 2, 0))
    out_lab = out_lab_base.transpose((1, 2, 0))

    # Convert IN_LAB image to RGB
    in_lab[:, :, 0:1] *= 100
    in_lab[:, :, 1:3] *= 255
    in_lab[:, :, 1:3] -= 128
    in_color_image = lab2rgb(in_lab.astype(np.float64)) * 255
    in_color_image = in_color_image.astype('uint8')
    in_color_image = Image.fromarray(in_color_image)

    # Convert OUT_LAB image to RGB
    out_lab[:, :, 0:1] *= 100
    out_lab[:, :, 1:3] *= 255
    out_lab[:, :, 1:3] -= 128
    out_color_image = lab2rgb(out_lab.astype(np.float64)) * 255
    out_color_image = out_color_image.astype('uint8')
    out_color_image = Image.fromarray(out_color_image)

    # Show RGB versions of OUT_LAB and IN_LAB
    in_l_image.show(title="L_IMAGE")
    in_color_image.show(title="IN_COLOUR")
    out_color_image.show(title="OUT_COLOUR")
