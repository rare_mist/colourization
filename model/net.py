"""Defines the neural network, losss function and metrics"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from torchvision import models


class Net(nn.Module):
    def __init__(self, params):
        super(Net, self).__init__()
        self.upsample_in = 128

        # First half: ResNet
        resnet = models.resnet18(num_classes=365)
        resnet.conv1.weight = nn.Parameter(
                resnet.conv1.weight.sum(dim=1).unsqueeze(1))
        # self.midlevel_resnet = nn.Sequential(*list(resnet.children())[0:6])
        self.half_resnet = nn.Sequential(*list(resnet.children())[0:6])

        self.upsample = nn.Sequential(
            nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Upsample(scale_factor=2),
            nn.Conv2d(128, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Upsample(scale_factor=2),
            nn.Conv2d(64, 32, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 2, kernel_size=3, stride=1, padding=1),
            nn.Upsample(scale_factor=2)
        )

    def forward(self, s):
        # Pass input through ResNet-gray to extract features
        half_resnet_features = self.half_resnet(s)

        # Upsample to get colors
        output = self.upsample(half_resnet_features)

        return output


def loss_fn(output_ab, input_ab):
    loss = nn.MSELoss()
    return loss(output_ab, input_ab)


def accuracy(outputs, labels):
    outputs = np.argmax(outputs, axis=1)
    return np.sum(outputs==labels)/float(labels.size)


# maintain all metrics required in this dictionary- these are used in the training and evaluation loops
metrics = {
    # 'accuracy': accuracy,
    # could add more metrics such as accuracy for each token type
}
