import os
import torch
import numpy as np

from PIL import Image
from skimage.color import rgb2lab, rgb2gray
from torchvision import datasets, transforms
from torch.utils.data import Dataset, DataLoader


# define a training image loader that specifies transforms on images. See
# documentation for more details.
train_transformer = transforms.Compose([
    transforms.RandomResizedCrop(224),
    transforms.RandomHorizontalFlip()])

# loader for evaluation, no horizontal flip
eval_transformer = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224)])


class PlacesDataset(Dataset):
    def __init__(self, data_dir, transform=None):
        self.root_dir = data_dir
        self.transform = transform
        self.filenames = os.listdir(data_dir)

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        # 1. Open image
        img_path = os.path.join(self.root_dir, self.filenames[idx])
        image = Image.open(img_path)

        # 2. Ensure image is RGB encoded
        if image.getbands()[0] == 'L':
            image = image.convert('RGB')

        if self.transform:
            # 3. Create base image
            #    - Apply image transformations
            #    - Change to numpy array format so we can use it with PIL
            #      functions
            base_image = self.transform(image)
            base_image = np.asarray(base_image)

            # 4. Create normalised AB image from RGB
            #    - Create LAB image from RGB
            #    - Normalize LAB image pixel values
            #    - Get AB channels from LAB image
            ab_image = rgb2lab(base_image)
            ab_image = (ab_image + 128) / 255
            ab_image = ab_image[:, :, 1:3]

            # 5. Convert normalised AB image to Tensor
            ab_image = torch.from_numpy(ab_image.transpose((2, 0, 1))).float()

            # 6. Create Grayscale image from original image
            l_image = rgb2gray(base_image)

            # 7. Convert Grayscale image to Tensor
            l_image = torch.from_numpy(l_image).unsqueeze(0).float()

        # Return grayscale l_image which will be network input and ab_image
        # which will act as label to compare network output with.
        return l_image, ab_image


def fetch_dataloader(types, data_dir, params):
    dataloaders = {}

    for split in ['train', 'val', 'test']:
        if split in types:
            path = os.path.join(data_dir, split)
            if split == 'train':
                dl = DataLoader(
                        PlacesDataset(path, train_transformer),
                        batch_size=params.batch_size,
                        shuffle=True,
                        num_workers=params.num_workers,
                        pin_memory=params.cuda)
            else:
                dl = DataLoader(
                        PlacesDataset(path, eval_transformer),
                        batch_size=params.batch_size,
                        shuffle=False,
                        num_workers=params.num_workers,
                        pin_memory=params.cuda)

            dataloaders[split] = dl

    return dataloaders
