import os
import sys
import utils
import torch
import logging
import argparse
import numpy as np
import model.net as net
import model.data_loader as data_loader

from torch.autograd import Variable


parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', default='data',
        help="Directory containing the dataset")
parser.add_argument('--model_dir', default='experiments/base_model',
        help="Directory containing params.json")
parser.add_argument('--restore_file', default='best',
        help="name of the file in --model_dir containing weights to load")


def evaluate(model, loss_fn, dataloader, metrics, params):
    # set model to evaluation mode
    model.eval()

    # summary for current eval loop
    summ = []

    # compute metrics over the dataset
    for i, (input_l, input_ab) in enumerate(dataloader):
        # move to GPU if available
        if params.cuda:
            input_l = input_l.cuda(async=True)
            input_ab = input_ab.cuda(async=True)

        # fetch the next evaluation batch
        input_l_batch = Variable(input_l)
        input_ab_batch = Variable(input_ab)

        # compute model output
        output_batch = model(input_l_batch)
        loss = loss_fn(output_batch, input_ab_batch)

        # extract data from torch Variable, move to cpu, convert to numpy arrays
        output_batch = output_batch.data.cpu().numpy()
        input_ab_batch = input_ab_batch.data.cpu().numpy()

        # compute all metrics on this batch
        summary_batch = {metric: metrics[metric](output_batch, labels_batch)
                         for metric in metrics}
        # summary_batch['loss'] = loss.data[0]
        summary_batch['loss'] = loss.item()
        summ.append(summary_batch)

    # compute mean of all metrics in summary
    metrics_mean = {
            metric:np.mean([x[metric] for x in summ]) for metric in summ[0]
    }
    metrics_string = " ; ".join(
            "{}: {:05.3f}".format(k, v) for k, v in metrics_mean.items()
    )
    logging.info("- Eval metrics : " + metrics_string)

    return metrics_mean


if __name__ == '__main__':
    # Load the parameters
    args = parser.parse_args()
    json_path = os.path.join(args.model_dir, 'params.json')
    tmp_str = "No json configuration file found at {}".format(json_path)
    assert os.path.isfile(json_path), tmp_str
    params = utils.Params(json_path)

    # use GPU if available
    params.cuda = torch.cuda.is_available()

    # Set the random seed for reproducible experiments
    # torch.manual_seed(230)
    # if params.cuda: torch.cuda.manual_seed(230)

    # Get the logger
    utils.set_logger(os.path.join(args.model_dir, 'evaluate.log'))

    # Create the input data pipeline
    logging.info("Creating the dataset...")

    # fetch dataloaders
    dataloaders = data_loader.fetch_dataloader(['test'], args.data_dir, params)
    test_dl = dataloaders['test']
    logging.info("- done.")

    # Define the model
    model = net.Net(params).cuda() if params.cuda else net.Net(params)
    loss_fn = net.loss_fn
    metrics = net.metrics

    logging.info("Starting evaluation")

    # Reload weights from the saved file
    tmp_chk_path = os.path.join(args.restore_file + '.pth.tar')
    utils.load_checkpoint(tmp_chk_path, model)

    # Evaluate
    test_metrics = evaluate(model, loss_fn, test_dl, metrics, params)
    tmp_str = "metrics_test_{}.json".format(args.restore_file)
    save_path = os.path.join(args.model_dir, tmp_str)
    utils.save_dict_to_json(test_metrics, save_path)
