import os
import torch
import utils
import logging
import argparse
import numpy as np
import model.net as net
import torch.optim as optim
import model.data_loader as data_loader

from tqdm import tqdm
from evaluate import evaluate
from torch.autograd import Variable


# Command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', help="Directory containing the dataset")
parser.add_argument('--model_dir', help="Directory containing params.json")


def train(model, optimizer, loss_fn, dataloader, metrics, params, output):
    # set model to training mode
    model.train()

    # summary for current training loop and a running average object for loss
    summ = []
    loss_avg = utils.RunningAverage()

    # Use tqdm for progress bar
    with tqdm(total=len(dataloader)) as t:
        for i, (input_l, input_ab) in enumerate(dataloader):
            # move to GPU if available
            if params.cuda:
                input_l = input_l.cuda(async=True)
                input_ab = input_ab.cuda(async=True)

            # convert to torch Variables
            input_l = Variable(input_l)
            input_ab = Variable(input_ab)

            # compute model output and loss
            output_ab = model(input_l)
            loss = loss_fn(output_ab, input_ab)

            # Print out some results every once in a while
            if output and i % params.save_summary_steps == 0:
                utils.print_result(output_ab, input_ab, input_l)

            # clear previous gradients, compute gradients of all variables wrt
            # loss
            optimizer.zero_grad()
            loss.backward()

            # performs updates using calculated gradients
            optimizer.step()

            # Evaluate summaries only once in a while
            if i % params.save_summary_steps == 0:
                # compute all metrics on this batch
                summary_batch = {
                        metric: metrics[metric](output_batch, labels_batch)
                        for metric in metrics
                }
                summary_batch['loss'] = loss.item()
                summ.append(summary_batch)

            # update the average loss
            loss_avg.update(loss.item())

            t.set_postfix(loss='{:05.3f}'.format(loss_avg()))
            t.update()

    # compute mean of all metrics in summary
    metrics_mean = {
            metric: np.mean([x[metric] for x in summ]) for metric in summ[0]
    }
    metrics_string = " ; ".join(
            "{}: {:05.3f}".format(k, v) for k, v in metrics_mean.items()
    )
    logging.info("- Train metrics: " + metrics_string)


def train_and_evaluate(model, train_dataloader, val_dataloader, optimizer,
                       loss_fn, metrics, params, model_dir, output):
    best_val_loss = 0.0

    for epoch in range(params.num_epochs):
        # Run one epoch
        logging.info("Epoch {}/{}".format(epoch + 1, params.num_epochs))

        # compute number of batches in one epoch (one full pass over the
        # training set)
        train(
            model, optimizer, loss_fn,
            train_dataloader, metrics,
            params, output)

        # Evaluate for one epoch on validation set
        val_metrics = evaluate(model, loss_fn, val_dataloader, metrics, params)

        val_loss = val_metrics['loss']

        if epoch == 0:
            is_best = True
        else:
            is_best = val_loss < best_val_loss

        # If best_eval, best_save_path
        if is_best:
            logging.info("- New best loss")
            best_val_loss = val_loss

            # Save best val metrics in a json file in the model directory
            best_json_path = os.path.join(
                    model_dir + 'checkpoints', "metrics_val_best_weights.json"
            )
            utils.save_dict_to_json(val_metrics, best_json_path)

            # Save weights
            utils.save_checkpoint({
                'epoch': epoch + 1, 'state_dict': model.state_dict(),
                'optim_dict': optimizer.state_dict()},
                is_best=is_best, checkpoint=model_dir + 'checkpoints')

        # Save latest val metrics in a json file in the model directory
        last_json_path = os.path.join(
                model_dir + 'checkpoints', "metrics_val_last_weights.json"
        )
        utils.save_dict_to_json(val_metrics, last_json_path)


if __name__ == '__main__':
    # Load the parameters from json file
    print("[INFO] - parse arguments")
    args = parser.parse_args()
    json_path = os.path.join(args.model_dir, 'params.json')
    assert os.path.isfile(json_path), "configuration file found: {}".format(
            json_path
    )
    _params = utils.Params(json_path)

    # use GPU if available
    _params.cuda = torch.cuda.is_available()
    print("[INFO] - parameters:")
    print("       - params.learning_rate:      ", _params.learning_rate)
    print("       - params.batch_size:         ", _params.batch_size)
    print("       - params.num_epochs:         ", _params.num_epochs)
    print("       - params.save_summary_steps: ", _params.save_summary_steps)
    print("       - params.num_workers:        ", _params.num_workers)
    print("       - params.cuda:               ", _params.cuda)

    # Set the logger
    utils.set_logger(os.path.join(args.model_dir, 'train.log'))

    # Create the input data pipeline
    logging.info("Loading the datasets...")

    # fetch dataloaders
    print("[INFO] - fetch dataloaders")
    dataloaders = data_loader.fetch_dataloader(
            ['train', 'val'], args.data_dir, _params
    )
    train_dl = dataloaders['train']
    val_dl = dataloaders['val']

    print("[INFO] - Train DataLoader attributes:")
    print("       - train_dl.dataset:         ", str(train_dl.dataset))
    print("       - len(train_dl.dataset):    ", len(train_dl.dataset))
    print("       - train_dl.batch_size:      ", train_dl.batch_size)
    print("       - train_dl.num_workers:     ", train_dl.num_workers)
    print("       - train_dl.collate_fn:      ", train_dl.collate_fn)
    print("       - train_dl.pin_memory:      ", train_dl.pin_memory)
    print("       - train_dl.drop_last:       ", train_dl.drop_last)
    print("       - train_dl.timeout:         ", train_dl.timeout)
    print("       - train_dl.worker_init_fn:  ", train_dl.worker_init_fn)
    logging.info("- done.")

    print("[INFO] - Define model")
    _model = net.Net(_params).cuda() if _params.cuda else net.Net(_params)

    print("[INFO] - Define optimizer:")
    _optimizer = optim.Adam(_model.parameters(), lr=_params.learning_rate)
    print(_optimizer)

    # fetch loss function and metrics
    print("[INFO] - Define loss function")
    _loss_fn = net.loss_fn

    print("[INFO] - Define metrics")
    _metrics = net.metrics

    # Train the model
    print("[INFO] - begin training")
    logging.info("Start training for {} epoch(s)".format(_params.num_epochs))
    train_and_evaluate(
        _model, train_dl, val_dl, _optimizer,
        _loss_fn, _metrics, _params, args.model_dir,
        False)
